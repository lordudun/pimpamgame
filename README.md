Code Kata del juego PimPam
---------------------------

Session sobre TDD, Mocking y Clean Code del PMA con Enrique Amodeo (@eamodeorubio)


-------------------------------------------------------------


Juego PimPam
------------

Entrada: Numero

Salida: Mensaje

Reglas del mensaje:

Pim: Divisible por 3 -> devuelve "Pim"

Pam: Divisible por 5 -> devuelve "Pam"
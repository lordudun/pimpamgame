package com.lordudun.pma.pimpam.juego.validador;

import java.util.ArrayList;
import java.util.List;

import com.lordudun.pma.pimpam.juego.Regla;
import com.lordudun.pma.pimpam.juego.ValidadorDeReglas;


public class ValidadorDeReglasDelJuegoPimPam implements ValidadorDeReglas {
	
	List<Regla> listaDeReglas;
	
	public ValidadorDeReglasDelJuegoPimPam() {
		
		listaDeReglas = new ArrayList<Regla>();
	}

    public void anadeNuevaRegla(Regla reglaAAnadir) {
		
		listaDeReglas.add(reglaAAnadir);
	}
    
    public String validarReglas(int numeroAValidar) {
		
		String mensaje = "";
		
		for(Regla siguienteRegla : listaDeReglas) {
			
			mensaje += siguienteRegla.validar(numeroAValidar);
		}
		
		return mensaje;
	}
}

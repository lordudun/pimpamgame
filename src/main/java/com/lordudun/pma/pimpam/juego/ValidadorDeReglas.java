package com.lordudun.pma.pimpam.juego;

public interface ValidadorDeReglas {

	 public void anadeNuevaRegla(Regla reglaAAnadir);
	    
	 public String validarReglas(int numeroAValidar);
}

package com.lordudun.pma.pimpam.juego;

public class JuegoPimPam {
	
	private ValidadorDeReglas reglasDelJuego;
	
	public JuegoPimPam(ValidadorDeReglas reglasDelJuego) {
		
		this.reglasDelJuego = reglasDelJuego;
	}
	
	public String juega(int numero) {
		
		return reglasDelJuego.validarReglas(numero);
	}
}

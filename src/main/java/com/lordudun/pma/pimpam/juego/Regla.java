package com.lordudun.pma.pimpam.juego;

public interface Regla {

	public String validar(int numeroAValidar);
}

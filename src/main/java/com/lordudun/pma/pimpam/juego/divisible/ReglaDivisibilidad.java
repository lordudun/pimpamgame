package com.lordudun.pma.pimpam.juego.divisible;

import com.lordudun.pma.pimpam.juego.*;

public class ReglaDivisibilidad implements Regla {

	private int divisor;
	private String mensajeCuandoLaReglaEsValida;
	
	public ReglaDivisibilidad(int divisor, String mensajeCuandoLaReglaEsValida) {
		
		this.divisor = divisor;
		this.mensajeCuandoLaReglaEsValida = mensajeCuandoLaReglaEsValida;
	}
	
	public String validar(int numeroAValidar){
		
		String mensaje = "";
		
		if (esDivisiblePor(divisor,numeroAValidar)) {
			
			mensaje = mensajeCuandoLaReglaEsValida;	
		}
		
		return mensaje;
	}

	private boolean esDivisiblePor(int divisor, int dividendo){
		
		return dividendo % divisor == 0;
	}
}

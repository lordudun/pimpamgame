package com.lordudun.pma.pimpam.test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.*;

import com.lordudun.pma.pimpam.juego.*;
import com.lordudun.pma.pimpam.juego.validador.ValidadorDeReglasDelJuegoPimPam;

public class Se_validan_todas_las_reglas {
	
	private String mensajeCualquiera_1 = "Mensaje1";
	private String mensajeCualquiera_2 = "Mensaje2";
	private int numeroCualquiera = 0;
	
	ValidadorDeReglas reglasDelJuegoPimPam;
	Regla mockedRegla_1 = mock(Regla.class);
	Regla mockedRegla_2 = mock(Regla.class);
	
	@Before
	public void inicializacion_del_test() {
		
		reglasDelJuegoPimPam = new ValidadorDeReglasDelJuegoPimPam();
		reglasDelJuegoPimPam.anadeNuevaRegla(mockedRegla_1);
	}
	
	@Test
	//Uso de Stub
	public void se_valida_una_regla_y_se_devuelve_su_mensaje() {
		
		when(mockedRegla_1.validar(anyInt())).thenReturn(mensajeCualquiera_1);
		assertEquals(mensajeCualquiera_1,reglasDelJuegoPimPam.validarReglas(numeroCualquiera));
	}
	
	@Test
	//Uso de Stub
	public void se_validan_las_dos_reglas_y_se_devuelven_sus_mensajes_en_orden() {
		
		String resultadoEsperado = mensajeCualquiera_1 + mensajeCualquiera_2;
		reglasDelJuegoPimPam.anadeNuevaRegla(mockedRegla_2);
		
		when(mockedRegla_1.validar(anyInt())).thenReturn(mensajeCualquiera_1);
		when(mockedRegla_2.validar(anyInt())).thenReturn(mensajeCualquiera_2);
		
		assertEquals(resultadoEsperado,reglasDelJuegoPimPam.validarReglas(numeroCualquiera));
	}
	
	@Test
	//Uso de Spy
	public void se_validan_las_reglas_con_los_parametros_correctos() {
		
		reglasDelJuegoPimPam.anadeNuevaRegla(mockedRegla_2);
		
		reglasDelJuegoPimPam.validarReglas(numeroCualquiera);
		
		verify(mockedRegla_1).validar(numeroCualquiera);
		verify(mockedRegla_2).validar(numeroCualquiera);
	}
}

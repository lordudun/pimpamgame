package com.lordudun.pma.pimpam.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import com.lordudun.pma.pimpam.juego.JuegoPimPam;
import com.lordudun.pma.pimpam.juego.ValidadorDeReglas;

public class Validar_el_juego_PimPam {
	
	private String mensajeCualquiera = "Mensaje";
	private int numeroCualquiera = 3;
	
	ValidadorDeReglas mockedReglasDelJuego = mock(ValidadorDeReglas.class);
	JuegoPimPam juegoPimPam;
	
	@Before
	public void inicializacion_del_test() {
		
		juegoPimPam = new JuegoPimPam(mockedReglasDelJuego);
	}
	
	@Test
	public void entra_un_numero_cualquiera_devuelve_Mensaje() {
		
		when(mockedReglasDelJuego.validarReglas(anyInt())).thenReturn(mensajeCualquiera);
		assertEquals(mensajeCualquiera,juegoPimPam.juega(numeroCualquiera));
	}
	
	@Test
	public void se_pide_validar_las_reglas_con_el_numero_enviado() {
		
		juegoPimPam.juega(numeroCualquiera);
		verify(mockedReglasDelJuego).validarReglas(numeroCualquiera);
	}
}

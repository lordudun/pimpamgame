package com.lordudun.pma.pimpam.test.divisible;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.lordudun.pma.pimpam.juego.divisible.ReglaDivisibilidad;

public class Si_cumple_divisibilidad_devuelve_el_mensaje {
	
	private ReglaDivisibilidad reglaDivisibilidad;
	
	private String mensajeCualquiera = "DivisiblePorTres";
	private int divisorCualquiera = 3;
	
	@Before
	public void inicializacion_del_test() {
		
		reglaDivisibilidad = new ReglaDivisibilidad(divisorCualquiera,mensajeCualquiera);
	}
	
	@Test
	public void entra_el_numero_divisor_devuelve_DivisiblePorTres() {
		
		assertEquals(mensajeCualquiera, reglaDivisibilidad.validar(divisorCualquiera));
	}
	
	@Test
	public void entra_el_numero_cinco_devuelve_una_cadena_vacia() {
		
		assertEquals("", reglaDivisibilidad.validar(5));
	}
	
	@Test
	public void entra_el_numero_divisor_por_dos_devuelve_DivisiblePorTres() {
		
		assertEquals(mensajeCualquiera, reglaDivisibilidad.validar(divisorCualquiera*2));
	}
}
